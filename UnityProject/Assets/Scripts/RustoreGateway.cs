using System;
using System.Collections.Generic;
using RuStore.BillingClient;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RustoreGateway : MonoBehaviour
{
    [SerializeField] private Button InitButton;
    [SerializeField] private Button CancelPreviousPurchaseButton;
    [SerializeField] private Button BuyButton;

    [SerializeField] private TMP_Text InitText;
    [SerializeField] private TMP_Text BuyText;

    private static List<Purchase> _purchasesInProgress;

    void Start()
    {
        DisableButtons();

        InitButton.gameObject.SetActive(true);
    }
    
    public void InitRuStoreSDK()
    {
        var config = new RuStoreBillingClientConfig()
        {
            consoleApplicationId = "5628863",
            deeplinkScheme = "deeplink.example.scheme",
            enableLogs = true,
            allowNativeErrorHandling = true
        };

        var initResult = RuStoreBillingClient.Instance.Init(config);

        if (initResult)
        {
            InitText.text = "RuStoreSDK init success.\nCheck in progress purchases...";
            InitButton.interactable = false;
            CheckInProgressPurchases();
        }
        else
        {
            InitText.text = "RuStoreBillingClient.Instance.Init() returns false";
        }
    }

    public void DeletePurchasesInProgress()
    {
        foreach (var purchase in _purchasesInProgress)
        {
            RuStoreBillingClient.Instance.DeletePurchase(purchase.purchaseId,
                _ => { InitText.text = $"DeletePurchase failed. Reason: {_.name},{_.description}"; },
                () =>
                {
                    InitText.text = "Purchases in progress were successfully deleted";
                    CancelPreviousPurchaseButton.interactable = false;
                    BuyButton.gameObject.SetActive(true);
                });
        }
    }

    public void BuyProduct()
    {
        InitText.text = string.Empty;
        BuyText.text = string.Empty;
        
        RuStoreBillingClient.Instance.PurchaseProduct("testing.1", 1, "", _ => { BuyText.text = $"Buy Failed. Reason: {_.name},{_.description}"; },
            _ => { BuyText.text = $"Buy Success"; });
    }

    private void CheckInProgressPurchases()
    {
        RuStoreBillingClient.Instance.GetPurchases(_ => { Debug.Log($"RustoreGateway GetPurchases failed. Reason: {_.name},{_.description}"); },
            purchasesInProgress =>
            {
                InitText.text = $"\nRuStoreBillingClient init success and returns {purchasesInProgress.Count} purchases in progress";
                if (purchasesInProgress.Count > 0)
                {
                    _purchasesInProgress = purchasesInProgress;
                    CancelPreviousPurchaseButton.gameObject.SetActive(true);
                }
                else
                {
                    BuyButton.gameObject.SetActive(true);
                }
            });
    }

    private void DisableButtons()
    {
        InitButton.gameObject.SetActive(false);
        CancelPreviousPurchaseButton.gameObject.SetActive(false);
        BuyButton.gameObject.SetActive(false);
    }
}
package unityplayeractivityextension;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import ru.rustore.unitysdk.billingclient.RuStoreUnityBillingClient;

import com.unity3d.player.UnityPlayer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RustoreIntentWrapper extends Activity {

    private static final String LOG_TAG = RustoreIntentWrapper.class
            .getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "ONCREATE. Intent: " + getIntent().toString());

        final String gameObjectName = "IntentFilter";
        final String methodName = "OnIntentFilter";

        /*
          Get the main activity launch it and wait until the unity player is
          ready to receive the URL via UnityPlayer.UnitySendMessage()
         */
        String myOwnPackageName = getApplicationContext().getPackageName();
        Intent mainLauncherIntent = getPackageManager()
                .getLaunchIntentForPackage(myOwnPackageName);
        startActivity(mainLauncherIntent);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_TAG, "onCreate Will now wait until unity player is ready..");
                while (UnityPlayer.currentActivity == null) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Log.d(LOG_TAG, "Unity is ready, sending event: "
                        + gameObjectName + "." + methodName + "("
                        + "msg" + ")");

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (savedInstanceState == null) {
                            Log.d(LOG_TAG, "Invoke RuStoreUnityBillingClient.onNewIntent");

                            RuStoreUnityBillingClient.onNewIntent(getIntent());
                        }

                        UnityPlayer.UnitySendMessage(gameObjectName, methodName,
                                "msg");
                    }
                });
            }
        });
    }
}